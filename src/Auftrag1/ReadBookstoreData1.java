package Auftrag1;

import javax.xml.parsers.*;
import org.w3c.dom.*;


public class ReadBookstoreData1 {

  public static void main(String[] args) {

    try {
    	// Name der Datei: "src/Auftrag1/buchhandlung.xml"
    	// Add your code here
    	DocumentBuilderFactory dBuilderFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dBuilderFactory.newDocumentBuilder();

      Document document = dBuilder.parse("src/Auftrag1/buchhandlung.xml");

      NodeList nodeList = document.getElementsByTagName("titel");
      Node titel = nodeList.item(0);

      System.out.println(titel.getTextContent());
    } catch (Exception e) {
      e.printStackTrace();
    } 

  }

}
