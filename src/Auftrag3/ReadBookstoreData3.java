package Auftrag3;

 /* Arbeitsauftrag:  Lesen Sie alle Angaben des Buches "Java ist auch eine Insel" 
 *					 aus der Datei "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *
 *                   Ausgabe soll wie folgt aussehen:
 *                    titel:  Java ist auch eine Insel   
 *					  vorname:  Christian 
 *                    nachname:  Ullenboom 
 */

import javax.xml.parsers.*;
import org.w3c.dom.*;

public class ReadBookstoreData3 {

	public static void main(String[] args) {

		try {
			// Name der Datei: "src/Auftrag3/buchhandlung.xml"
			// Add your code here
			DocumentBuilderFactory dBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dBuilderFactory.newDocumentBuilder();
			Document document = dBuilder.parse("src/Auftrag3/buchhandlung.xml");

			NodeList tList = document.getElementsByTagName("titel");
			Node titel1 = tList.item(0);

			NodeList vList = document.getElementsByTagName("vorname");
			Node vNode1 = vList.item(0);

			NodeList nList = document.getElementsByTagName("nachname");
			Node nNode1 = nList.item(0);

			System.out.println("Titel: " + titel1.getTextContent());
			System.out.println("Vorname: " + vNode1.getTextContent());
			System.out.println("Nachname: " + nNode1.getTextContent());
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} 

	}

}
