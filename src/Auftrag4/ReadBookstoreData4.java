package Auftrag4;

/* Arbeitsauftrag:  Lesen Sie nur die Buchtitel 
 *					aus der Datei "buchhandlung.xml" und geben Sie sie 
 * 					auf dem Bildschirm aus.
 * 
 *                  Ausgabe soll wie folgt formatiert werden:
 *                     1. titel: Everyday Italian
 *                     2. titel: Harry Potter
 *                     3. titel: XQuery Kick Start
 *                     4. titel: Learning XML
 *                     
 * Hinweis: Sie ben�tigen ein NodeList-Objekt und eine Schleife, die diese iteriert!
 */


 import javax.xml.parsers.*;
 import org.w3c.dom.*;


public class ReadBookstoreData4 {

	public static void main(String[] args) throws NullPointerException {

			// Name der Datei: "src/Auftrag4/buchhandlung.xml"
			// Add your code here
		
		try {
			DocumentBuilderFactory dBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dBuilderFactory.newDocumentBuilder();
			Document document = dBuilder.parse("./src/Auftrag4/buchhandlung.xml");

			NodeList titel = document.getElementsByTagName("titel");

			for (int i = 0; i<4; i++)
			{
				Node node = titel.item(i);
				System.out.println(i+1 + ". titel: " + node.getTextContent());
			}


		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
