package Auftrag5;

 /* Arbeitsauftrag:  Lesen Sie nur die Autoren des Buches "XQuery Kick Start" aus der Datei  
 *                  "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *                  
 *                   Ausgabe soll wie folgt aussehen:
 *                     Buchtitel:  XQuery Kick Start
 *                     Autoren: 
 *                     	    1. autor: James McGovern
 *                          2. autor: Per Bothner
 *                          3. autor: Kurt Cagle
 *                          4. autor: James Linn
 *                          5. autor: Vaidyanathan Nagarajan
 *                          
 * Hinweis: Sie ben�tigen ein NodeList-Objekt und eine Schleife, die diese iteriert!
 */

 import javax.xml.parsers.*;
 import org.w3c.dom.*;



public class ReadBookstoreData5 {

	public static void main(String[] args) {

		
			// Name der Datei: "src/Auftrag5/buchhandlung.xml"
			// Add your code here
		try {
			DocumentBuilderFactory dBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dBuilderFactory.newDocumentBuilder();
			Document document = dBuilder.parse("./src/Auftrag5/buchhandlung.xml");

			NodeList titelList = document.getElementsByTagName("titel");

			System.out.println("Buchtitel: " + titelList.item(2).getTextContent());
			NodeList autorList = document.getElementsByTagName("autor");

			for(int i = 2; i<6; i++) {
				System.out.println(i-1 + ". autor: " + autorList.item(i).getTextContent());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
