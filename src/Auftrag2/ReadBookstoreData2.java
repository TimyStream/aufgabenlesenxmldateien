package Auftrag2;

/* Arbeitsauftrag:  Lesen Sie den Titel und den Autor des Buches "Java ist auch eine Insel" 
*					 aus der Datei "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
*
*                   Ausgabe soll wie folgt aussehen:
*                        titel:  Java ist auch eine Insel 
*                        autor:  Max Mustermann 
*/

import javax.xml.parsers.*;
import org.w3c.dom.*;

public class ReadBookstoreData2 {

	public static void main(String[] args) {

		try {
			// Name der Datei: "src/Auftrag2/buchhandlung.xml"
			// Add your code here

			DocumentBuilderFactory dBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dBuilderFactory.newDocumentBuilder();
			Document document = dBuilder.parse("src/Auftrag2/buchhandlung.xml");

			NodeList tList = document.getElementsByTagName("titel");
			Node titel1 = tList.item(0);

			NodeList aList = document.getElementsByTagName("autor");
			Node autor1 = aList.item(0);

			System.out.println("Titel: " + titel1.getTextContent());
			System.out.println("Autor: " + autor1.getTextContent());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
